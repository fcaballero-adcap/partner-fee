package banza.partner.productores;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.rdsdata.AWSRDSData;
import com.amazonaws.services.rdsdata.AWSRDSDataClientBuilder;
import com.amazonaws.services.rdsdata.model.ExecuteStatementRequest;
import com.amazonaws.services.rdsdata.model.ExecuteStatementResult;
import com.amazonaws.services.rdsdata.model.Field;

import banza.partner.productores.dto.FeeDto;
import banza.partner.productores.dto.FeeResponseDto;
import banza.partner.productores.dto.Params;

public class HandlerFee implements RequestHandler<Params, FeeResponseDto> {

	@Override
	public FeeResponseDto handleRequest(Params event, Context context) {
		AWSRDSData rdsData = AWSRDSDataClientBuilder.standard()
				.withEndpointConfiguration(
						new AwsClientBuilder.EndpointConfiguration(System.getenv("ENDPOINT"), System.getenv("REGION")))
				.build();
		ExecuteStatementRequest request = new ExecuteStatementRequest().withResourceArn(System.getenv("RESOURCE_ARN"))
				.withSecretArn(System.getenv("SECRET_ARN")).withDatabase(System.getenv("BD"))
				.withSql(String.format(System.getenv("QUERY"), event.getIdProductor()));
		ExecuteStatementResult result = rdsData.executeStatement(request);

		FeeDto fee = new FeeDto();
		if (!result.getRecords().isEmpty()) {
			Calendar c = Calendar.getInstance();
			Date now = c.getTime();
			c.add(Calendar.DATE, -7);
			Date nowMinus7 = c.getTime();
			c.add(Calendar.DATE, -23);
			Date nowMinus30 = c.getTime();
			c.add(Calendar.DATE, -30);
			Date nowMinus60 = c.getTime();
			LocalDate localDate = now.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			int yearNow = localDate.getYear();
			for (List<Field> fields : result.getRecords()) {
				Date fecha = null;
				try {
					fecha = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(fields.get(0).getStringValue());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				LocalDate dateFee = fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				int yearFee = dateFee.getYear();
				if (fecha.before(now) && fecha.after(nowMinus7)) {
					fee.setTotalSemana(fee.getTotalSemana().add(new BigDecimal(fields.get(2).getStringValue())));
				} else if (fecha.after(nowMinus30)) {
					fee.setTotalMes(fee.getTotalMes().add(new BigDecimal(fields.get(2).getStringValue())));
				} else if (fecha.after(nowMinus60)) {
					fee.setTotalDosMeses(fee.getTotalDosMeses().add(new BigDecimal(fields.get(2).getStringValue())));
				} else if (yearNow == yearFee) {
					fee.setTotalAnio(fee.getTotalAnio().add(new BigDecimal(fields.get(2).getStringValue())));
				}
			}
			fee.setTotalMes(fee.getTotalMes().add(fee.getTotalSemana()).setScale(2, BigDecimal.ROUND_HALF_UP));
			fee.setTotalDosMeses(fee.getTotalDosMeses().add(fee.getTotalMes()).setScale(2, BigDecimal.ROUND_HALF_UP));
			fee.setTotalAnio(fee.getTotalAnio().add(fee.getTotalDosMeses()).setScale(2, BigDecimal.ROUND_HALF_UP));
		}
		FeeResponseDto feeResponse = new FeeResponseDto();
		feeResponse.setTotalSemana(fee.getTotalSemana().toString());
		feeResponse.setTotalMes(fee.getTotalMes().toString());
		feeResponse.setTotalDosMeses(fee.getTotalDosMeses().toString());
		feeResponse.setTotalAnio(fee.getTotalAnio().toString());
		return feeResponse;
	}
}
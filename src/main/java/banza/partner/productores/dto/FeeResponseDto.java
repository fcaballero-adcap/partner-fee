package banza.partner.productores.dto;

public class FeeResponseDto {

	private String totalSemana;
	private String totalMes;
	private String totalDosMeses;
	private String totalAnio;

	public String getTotalSemana() {
		return totalSemana;
	}

	public String getTotalMes() {
		return totalMes;
	}

	public String getTotalDosMeses() {
		return totalDosMeses;
	}

	public String getTotalAnio() {
		return totalAnio;
	}

	public void setTotalSemana(String totalSemana) {
		this.totalSemana = totalSemana;
	}

	public void setTotalMes(String totalMes) {
		this.totalMes = totalMes;
	}

	public void setTotalDosMeses(String totalDosMeses) {
		this.totalDosMeses = totalDosMeses;
	}

	public void setTotalAnio(String totalAnio) {
		this.totalAnio = totalAnio;
	}

}
package banza.partner.productores.dto;

import java.math.BigDecimal;
import java.util.Date;

public class FeeDto {

	private BigDecimal totalSemana;
	private BigDecimal totalMes;
	private BigDecimal totalDosMeses;
	private BigDecimal totalAnio;

	public FeeDto() {
		super();
		this.totalSemana = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
		this.totalMes = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
		this.totalDosMeses = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
		this.totalAnio = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	public BigDecimal getTotalSemana() {
		return totalSemana;
	}

	public BigDecimal getTotalMes() {
		return totalMes;
	}

	public BigDecimal getTotalDosMeses() {
		return totalDosMeses;
	}

	public BigDecimal getTotalAnio() {
		return totalAnio;
	}

	public void setTotalSemana(BigDecimal totalSemana) {
		this.totalSemana = totalSemana;
	}

	public void setTotalMes(BigDecimal totalMes) {
		this.totalMes = totalMes;
	}

	public void setTotalDosMeses(BigDecimal totalDosMeses) {
		this.totalDosMeses = totalDosMeses;
	}

	public void setTotalAnio(BigDecimal totalAnio) {
		this.totalAnio = totalAnio;
	}



	private Date fecha;
	private Long total_productor;

	public Date getFecha() {
		return fecha;
	}

	public Long getTotal_productor() {
		return total_productor;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public void setTotal_productor(Long total_productor) {
		this.total_productor = total_productor;
	}

}